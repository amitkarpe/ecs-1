# Example using the aws-ecs-deploy pipe to deploy to AWS Elastic Container Service
## How to use this repo

1. To start you need to fork a repo.
2. You'll also need and AWS account with an [ECS service](https://aws.amazon.com/ecs/) set up and running.
3. An AWS access key with permissions associated to execute the RegisterTaskDefinition and UpdateService actions.
4. Enable Pipelines in your repo and congirue the [repository variables](https://confluence.atlassian.com/bitbucket/variables-in-pipelines-794502608.html#Variablesinpipelines-Repositoryvariables)
5. You can keep the ECS_CLUSTER_NAME, ECS_SERVICE_NAME and ECS_TASK_FAMILY_NAME as is. In this case you should have a cluster, service and task definition with the corresponding names in ECS. You can also replace them with your own values.

Input:

```
Preparing service : 9 of 9 complete
ECS resource creation
complete
Cluster ecs-cluster
complete
Task definition example-ecs-app:8
complete
Service ecs-service
complete
Additional AWS service integrations
complete
Log group The log group [ /ecs/example-ecs-app ] already exists
complete
CloudFormation stack EC2ContainerService-ecs-cluster
complete
VPC vpc-0457828c8d5a07148
complete
Subnet 1 subnet-05a9f329442d8cf92
complete
Subnet 2 subnet-024b29472aa91d308
complete
Security group sg-0c84af69053b7ada2
complete 
```

Output:
```
Status: Downloaded newer image for bitbucketpipelines/aws-ecs-deploy:1.0.0
INFO: Updating the task definition...
INFO: Found credentials in environment variables.
INFO: Using task definition: 
{'containerDefinitions': [{'essential': True,
                           'image': 'amitkarpe/ecs-1:1',
                           'name': 'node-app',
                           'portMappings': [{'containerPort': 3000,
                                             'hostPort': 3000,
                                             'protocol': 'tcp'}]}],
 'cpu': '256',
 'family': 'example-ecs-app',
 'memory': '512',
 'networkMode': 'awsvpc',
 'requiresCompatibilities': ['FARGATE']}
INFO: Update the ecs-service service.
✔ Successfully updated the ecs-service service. You can check you servce here: 
https://console.aws.amazon.com/ecs/home?region=ap-southeast-1#/clusters/ecs-cluster/services/ecs-service/details
```

Testing:

```
Status: Downloaded newer image for amitkarpe/ecs-1:1
docker.io/amitkarpe/ecs-1:1
[cloud_user@amitkarpe1c ~]$ docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
amitkarpe/ecs-1     1                   c9f8a39de4eb        6 minutes ago       937MB
...
[cloud_user@amitkarpe1c ~]$ docker run --rm -p 3002:3000 -d amitkarpe/ecs-1:1
98132e453d44b6eee794bc11a8d01ce9019f7afe197fb090c61697322b09cde6
[cloud_user@amitkarpe1c ~]$ docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
98132e453d44        amitkarpe/ecs-1:1   "docker-entrypoint.s…"   29 seconds ago      Up 6 seconds        0.0.0.0:3002->3000/tcp   gallant_bhaskara

[cloud_user@amitkarpe1c ~]$ curl localhost:3002
Hello World!
[cloud_user@amitkarpe1c ~]$ curl localhost:3002
Hello World!
[cloud_user@amitkarpe1c ~]$
```